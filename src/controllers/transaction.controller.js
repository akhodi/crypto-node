const fs = require('fs');
const axios = require('axios');
const csv = require('csv-parser');
const httpStatus = require('http-status');

const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');

const isEmpty = (obj) => {
  return Object.keys(obj).length === 0;
};

const getDatetime = (date) => {
  const today = new Date(date);
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0');
  const yyyy = today.getFullYear();

  return `${yyyy}-${mm}-${dd}`;
};

const getLogTransactions = async (fileName, price) => {
  return new Promise(function (resolve, reject) {
    const results = [];
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.createReadStream(fileName)
      .pipe(csv())
      .on('data', (row) => {
        results.push({
          ...row,
          timestamp: parseInt(row.timestamp, 10),
          datetime: new Date(row.timestamp * 1000),
          amount: parseFloat(row.amount),
          amount_usd: parseFloat(
            (row.amount * price[row.token].USD).toFixed(2),
          ),
        });
      })
      .on('end', () => {
        resolve(results);
      })
      .on('error', reject);
  });
};

const getTransactions = catchAsync(async (req, res) => {
  const query = pick(req.query, ['token', 'date']);

  const data = await axios
    .get('https://min-api.cryptocompare.com/data/pricemulti', {
      params: {
        fsyms: 'BTC,ETH,XRP',
        tsyms: 'USD',
      },
    })
    .then(async ({ data: resData }) => {
      let results = await getLogTransactions('transactions.csv', resData);

      if (!isEmpty(query)) {
        results = results.filter((t) => {
          if (query.token && query.date) {
            return (
              t.token === query.token.toUpperCase() &&
              getDatetime(t.datetime) === query.date
            );
          }

          if (query.date) {
            return getDatetime(t.datetime) === query.date;
          }

          return t.token === query.token && query.token.toUpperCase();
        });
      }

      return results;
    });

  const response = {
    code: httpStatus.OK,
    message: 'OK',
    data,
  };

  res.send(response);
});

module.exports = { getTransactions };
