const express = require('express');
const transactionController = require('../../controllers/transaction.controller');

const router = express.Router();

router.route('/').get(transactionController.getTransactions);

module.exports = router;
